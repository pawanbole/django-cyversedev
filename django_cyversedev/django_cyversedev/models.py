from django.db import models

class AppDetails(models.Model):
        id = models.TextField(max_length= 5000, primary_key=True)
        description = models.TextField(max_length= 5000)
        Name = models.TextField(max_length= 5000)
        Formats = models.TextField(max_length= 5000)

        class Meta:
            db_table = "AppDetails"


class InputDetails(models.Model):
        id = models.TextField(max_length= 5000, primary_key=True)
        AppId = models.TextField(max_length= 5000)
        Description = models.TextField(max_length= 5000)
        label = models.TextField(max_length= 5000)
        Inputtype = models.TextField(max_length= 5000)
        Formtype = models.TextField(max_length= 5000)

        class Meta:
            db_table = "InputDetails"


class Arguments(models.Model):
        id = models.TextField(max_length= 5000, primary_key=True)
        Name = models.TextField(max_length= 5000)
        isDefault = models.BooleanField()
        Display = models.TextField(max_length= 5000)
        value = models.TextField(max_length= 5000)
        InputDetailId = models.TextField(max_length= 5000)


        class Meta:
            db_table = "Arguments"
