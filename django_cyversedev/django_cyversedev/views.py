import json

from . import settings
from django.http import HttpResponse, HttpResponseRedirect
from .getSpeciesVersionList import getSpeciesVersionList
from .terrain.getUserData import getUserData
from .terrain.getSharedData import getSharedData
from .terrain.getCommunityData import getCommunityData
from .terrain.getUserDetail import getUserDetails
from .terrain.getUserDetail import getUserDetailJson
from .terrain.searchForFile import searchForFile
from .terrain.searchUser import searchForUser
from .terrain.getMetadata import getMetadata
from .terrain.saveMetadata import saveMetadata
from .terrain.getBasicAuth import getBasicAuth
from .terrain.shareFile import shareFile
from .terrain.unshareFile import unshareFile
from .terrain.checkFilePermissions import checkFilePermissions
from .terrain.getSessionUserData import getSessionIdfromCookie
from .terrain.getSessionUserData import getaccesstokenfromredis
from .terrain.getSessionUserData import getUserNamefromSessionData
from .terrain.getSessionUserData import getCommunityData_Prefix
from .terrain.getSessionUserData import getHomePath_Prefx
from .terrain.getAnalysisHistory import getAnalysisHistory
from .terrain.casLogin import casLogin
from django.shortcuts import render
from .terrain.getAccessToken import getAccessToken
from .terrain.getBasicAuth import getBasicAuth
from .terrain.ManageAppDetails import *
import requests
from django.views.decorators.csrf import csrf_exempt
from django.core.cache import cache
import uuid
import sys


def index(request):
    context = {}
    return render(request, 'index.html', context)


def home(request):
    context = {}
    return render(request, 'django_cyversedev/middlepanel.html', context)


def saveSessionRedis(accesstoken):
    session_id = str(uuid.uuid4())
    userdata = getUserDetailJson(accesstoken)
    cache.set(session_id, [accesstoken, userdata], timeout=settings.REDIS_EXPIRY)
    return session_id


def loadlogin(request):
    context = {}
    print("Development Environment: "+settings.IS_DEV_ENVIRONMENT)
    if False:
        accesstoken = getBasicAuth()
        session_id = saveSessionRedis(accesstoken)
        response = HttpResponseRedirect('middlepanel/')
        response.set_cookie(key='sessionId', value=session_id)
        return response
    return render(request, 'login.html', context)


def login(request):
    if request.is_ajax():
        try:
            redirectUrl = casLogin()
        except:
            e = sys.exc_info()
            return HttpResponse(e)
    return HttpResponse(redirectUrl, content_type='text/plain')


def loginCyverse(request):
    userData = getUserData('', '20', '0', 'NAME', 'ASC', '', '')
    context = {
        "userData": userData
    }
    return render(request, 'base.html', context)


def oauth(request):
    context = {}
    return render(request, 'oauth.html', context)


def getSessionId(request):
    code = request.GET.get('code')
    access_token = getAccessToken(code)
    sessionId = saveSessionRedis(access_token)
    response = HttpResponse('')
    response.set_cookie(key='sessionId', value=sessionId)
    return response


def getSessionData(sessionid):
    return cache.get(sessionid)


def base(request):
    context = {}
    return render(request, 'middlepanel.html', context)


def middlepanel(request):
    response = None
    if request.is_ajax():
        try:
            # sessionid = request.GET.get('sessionid')
            path = request.GET.get('url')
            limit = request.GET.get('limit')
            offset = request.GET.get('offset')
            sortcol = request.GET.get('sortcol')
            sortdir = request.GET.get('sortdir')
            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            response = getUserData(path, limit, offset, sortcol, sortdir, redis_sessiondata)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)


def renderSharedData(request):
    response = None
    if request.is_ajax():
        try:
            # sessionid = request.GET.get('sessionid')
            path = request.GET.get('url')
            limit = request.GET.get('limit')
            offset = request.GET.get('offset')
            sortcol = request.GET.get('sortcol')
            sortdir = request.GET.get('sortdir')
            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            response = getSharedData(path, limit, offset, sortcol, sortdir, redis_sessiondata)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)


def getFileIdApi(request):
    response = None
    if request.is_ajax():
        try:
            path = request.GET.get('url')
            if path is None:
                raise Exception("The request header does not contain Path")
            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            accesstoken = getaccesstokenfromredis(redis_sessiondata)
            response = getMetadata(path, accesstoken)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'cyverseData.html', context)


def retrieveMetaData(request):
    response = None

    if request.is_ajax():
        try:
            fileId = request.GET.get('fileId')
            if fileId is None:
                raise Exception("The request header does not contain fileId")
            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            accesstoken = getaccesstokenfromredis(redis_sessiondata)
            response = getMetadata(fileId, accesstoken)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'cyverseData.html', context)


@csrf_exempt
def saveMetaData(request):
    # fileId = ""
    # data_attrib = [{"attr":"Genome","unit":"integratedGenomeBrowser","value":"somegenome"},{"attr":"foreground","unit":"integratedGenomeBrowser","value":"1211212"},{"attr":"background","unit":"integratedGenomeBrowser","value":"1211212"}]
    response = None
    if request.is_ajax():
        try:
            fileId = request.POST['fileId']
            data_attrib = json.loads(request.POST['data'])
            if fileId is None:
                raise Exception("The request header does not contain fileId")
            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            accesstoken = getaccesstokenfromredis(redis_sessiondata)
            response = saveMetadata(fileId, data_attrib, accesstoken)

        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'cyverseData.html', context)


def getCommunityDataList(request):
    response = None
    if request.is_ajax():
        try:
            # sessionid = request.GET.get('sessionid')
            path = request.GET.get('url')
            limit = request.GET.get('limit')
            offset = request.GET.get('offset')
            sortcol = request.GET.get('sortcol')
            sortdir = request.GET.get('sortdir')
            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            response = getCommunityData(path, limit, offset, sortcol, sortdir, redis_sessiondata)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)


def setPublicLink(request):
    response = None
    if request.is_ajax():
        try:
            path = request.GET.get('url')
            is_anonymous = request.GET.get('is_anonymous')

            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            accesstoken = getaccesstokenfromredis(redis_sessiondata)
            username = getUserNamefromSessionData(redis_sessiondata)
            if (bool(is_anonymous)):
                username = "anonymous"
            response = shareFile(path, username, accesstoken)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)


def getRootPathUser(request):
    response = None
    if request.is_ajax():
        try:
            sessionId = getSessionIdfromCookie(request.COOKIES)
            redis_data = getSessionData(sessionId)
            accesstoken = getaccesstokenfromredis(redis_data)
            response = getUserDetails(accesstoken)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)


def searchFile(request):
    response = None
    if request.is_ajax():
        try:
            exact = request.GET.get('exact')
            label = request.GET.get('label')
            size = int(request.GET.get('limit'))
            type = request.GET.get('type')
            offset = int(request.GET.get('offset'))
            sort_field = request.GET.get('sortfield')
            sort_order = request.GET.get('sortorder')
            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            accesstoken = getaccesstokenfromredis(redis_sessiondata)
            username = getUserNamefromSessionData(redis_sessiondata)
            home_path_prefix = getHomePath_Prefx(username, redis_sessiondata)
            community_path_prefix = getCommunityData_Prefix(username, redis_sessiondata)
            response = searchForFile(exact, label, size, type, offset, sort_field, sort_order, accesstoken, username, home_path_prefix, community_path_prefix)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)


def remPublicLink(request):
    response = None
    if request.is_ajax():
        try:
            # sessionid = request.GET.get('sessionid')
            path = request.GET.get('url')
            is_anonymous = request.GET.get('is_anonymous')
            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            accesstoken = getaccesstokenfromredis(redis_sessiondata)
            username = getUserNamefromSessionData(redis_sessiondata)
            if (bool(is_anonymous)):
                username = "anonymous"
            response = unshareFile(path, username, accesstoken)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)


def checkFilePermission(request):
    response = None
    if request.is_ajax():
        try:
            # sessionid = request.GET.get('sessionid')
            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            path = request.GET.get('url')
            response = checkFilePermissions(path, redis_sessiondata)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        context = {}
        return render(request, 'middlepanel.html', context)


def searchtheUser(request):
    response = None
    if request.is_ajax():
        try:
            label = request.GET.get('label')
            sessionid = getSessionIdfromCookie(request.COOKIES)
            redis_sessiondata = getSessionData(sessionid)
            accesstoken = getaccesstokenfromredis(redis_sessiondata)
            response = searchForUser(label, accesstoken)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)


def getSpeciesVersion(request):
    response = None
    if request.is_ajax():
        try:
            response = getSpeciesVersionList()
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'rightpanel.html', context)


def downloadFile(request):
    response = None
    try:
        filepath = request.GET.get('path')
        req_url = 'https://de.cyverse.org/terrain/secured/fileio/download?path=' + filepath
        r = requests.get(req_url, headers={'Authorization': 'BEARER '+settings.ACCESS_TOKEN})
        return HttpResponse(r)
    except:
        e = sys.exc_info()
        return HttpResponse(e)
def Signout(request):
    response = None
    try:
        sessionid = getSessionIdfromCookie(request.COOKIES)
        cache.delete(sessionid)
        response = HttpResponse('')
        response.delete_cookie(key='sessionId')
        return response
    except:
        e = sys.exc_info()
        return HttpResponse(e)
def getAnalyze(request):
    response = None
    sessionid = getSessionIdfromCookie(request.COOKIES)
    print(sessionid)
    redis_sessiondata = getSessionData(sessionid)
    try:
        response = getAnalysisHistory("100","0","enddate","ASC",redis_sessiondata)
    except:
    	e = sys.exc_info()
    	return HttpResponse(e)
    print(response)
    return HttpResponse(response, content_type='application/json')

def SyncAppDataToBioViz(request):
    accessToken = request.GET.get('accesstoken')
    try:
       response_json = getLatestApps(accessToken)
    except:
        e = sys.exc_info()
        return HttpResponse(e)
    return HttpResponse(response_json)
