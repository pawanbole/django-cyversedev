class FileSystemInfo:
    def __init__(self):
        self.infoType = ""
        self.path = ""
        self.dateCreated = ""
        self.permission = ""
        self.dateModified = ""
        self.fileSize = ""
        self.badName = False
        self.isFavorite = False
        self.label = ""
        self.id = 0
