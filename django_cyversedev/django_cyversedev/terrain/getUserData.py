#!/usr/bin/env python
import requests
from .. import settings
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder
from ..terrain.getSessionUserData import gethomePathfromSessionData

def getUserData(path, limit, offset, sortcol, sortdir, redis_sessiondata):
    accesstoken = str(redis_sessiondata[0])
    json_data_user = str(redis_sessiondata[1])
    home_path = gethomePathfromSessionData(json_data_user)
    r = requests.get(
        settings.BASE_URL + '/secured/filesystem/paged-directory?path='+path+'&limit=' + limit + '&offset=' + offset + '&sort-col=' + sortcol + '&sort-dir=' + sortdir,
        headers={'Authorization': 'Bearer ' + accesstoken})
    json_data = r.json()
    response = ResponseParser.get_files(json_data)
    custom_encoder = CustomEncoder()
    res = custom_encoder.encode(response)
    return res
