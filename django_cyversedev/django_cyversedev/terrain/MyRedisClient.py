#!/usr/bin/env python

import redis
from django_cyversedev.django_cyversedev import settings


class RedisClient:

    def __init__(self):
        rpassword = settings.REDIS_PASSWORD
        self.pool = redis.ConnectionPool(host='127.0.0.1', port=6379, db=0, password=rpassword)
        self._conn = None

    def conn(self):
        if self._conn is None:
            self._conn = redis.Redis(connection_pool=self.pool)
        return self._conn
