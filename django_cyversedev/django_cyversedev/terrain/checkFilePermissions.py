# check file permissions
import requests
from .. import settings
from ..terrain_model.RequestCreator import RequestCreator
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder


def checkFilePermissions(filepath, redis_sessiondata):
    # Multiple filepaths can be send separated by ','
    accesstoken = str(redis_sessiondata[0])
    request = RequestCreator.create_file_permission_request(filepath)
    req_url = 'https://de.cyverse.org/terrain/secured/filesystem/user-permissions'
    r = requests.post(req_url, headers={'Authorization': 'BEARER ' + accesstoken,
                                        'Content-type': 'application/json'},
                      data=request)
    response = ResponseParser.parse_file_permission_response(r.json())
    custom_encoder = CustomEncoder()
    res = custom_encoder.encode(response)
    return res

