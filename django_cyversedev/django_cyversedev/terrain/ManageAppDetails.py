import requests
from .. import models

def DeleteAppData():
    models.Arguments.objects.all().delete()
    models.InputDetails.objects.all().delete()
    models.AppDetails.objects.all().delete()


def getLatestApps(accesstoken):
    DeleteAppData()
    req_url = 'https://de.cyverse.org/terrain/apps/communities/iplant:de:prod:communities:Integrated Genome Browser/apps'
    r = requests.get(req_url, headers={'Authorization': 'BEARER ' + accesstoken})
    add_IGBApps(r.json(), accesstoken)
    return True

def formatFileTypes_fromdescription(descrption):
    start_indexFileType = descrption.find('%INPUT%')
    if start_indexFileType == -1:
        return ""
    return descrption[start_indexFileType+7:]

def get_InputDetailsForApp(appId, accesstoken):
    req_url = 'https://de.cyverse.org/terrain/apps/de/'+appId
    r = requests.get(req_url, headers={'Authorization': 'BEARER ' + accesstoken})
    return r.json()

def Add_InputDetailsForApp(AppId, accesstoken):
    app_description = get_InputDetailsForApp(AppId, accesstoken)
    for group in app_description['groups']:
        label = group['label']
        for parameter in group['parameters']:
            inputdetails = models.InputDetails()
            inputdetails.Description = parameter['description']
            inputdetails.Formtype = parameter['type']
            inputdetails.AppId = AppId
            inputdetails.id = parameter['id']
            inputdetails.label = parameter['label']
            inputdetails.Inputtype = label
            inputdetails.save()
            if len(parameter['arguments'])>0:
                for argument in parameter['arguments']:
                    arguments = models.Arguments()
                    arguments.id = argument['id']
                    arguments.isDefault = bool(argument['isDefault'])
                    arguments.Name = argument['name']
                    arguments.value = argument['value']
                    arguments.Display = argument['display']
                    arguments.InputDetailId = inputdetails.id
                    arguments.save()

def add_IGBApps(json, accesstoken):
    for app_json in json['apps']:
        app = models.AppDetails()
        app.id = app_json['id']
        app.Name = app_json['name']
        app.description = app_json['description']
        app.Formats = formatFileTypes_fromdescription(app_json['description'])
        app.save()
        Add_InputDetailsForApp(app.id, accesstoken)
