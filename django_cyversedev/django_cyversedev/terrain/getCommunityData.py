# get community data
import requests
from .. import settings
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder


def getCommunityData(filepath, limit, offset, sortcol, sortdir, redis_sessiondata):
    accesstoken = str(redis_sessiondata[0])
    json_data_user = str(redis_sessiondata[1])

    req_url = 'https://de.cyverse.org/terrain/secured/filesystem/paged-directory?path='+filepath+'&limit='+limit+'&offset='+offset+'&sort-col='+sortcol+'&sort-dir='+sortdir
    r = requests.get(req_url, headers={'Authorization': 'BEARER ' + accesstoken})
    json_data = r.json()
    response = ResponseParser.get_files(json_data)
    custom_encoder = CustomEncoder()
    res = custom_encoder.encode(response)
    return res

