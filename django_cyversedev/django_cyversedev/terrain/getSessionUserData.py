from ..terrain_model.ResponseParser import ResponseParser

def gethomePathfromSessionData(json_data):
    response = ResponseParser.parse_getUserDetails(json_data)
    homepath = response.base_paths.user_home_path
    return homepath

def getSessionIdfromCookie(cookie):
    return str(cookie['sessionId'])

def getaccesstokenfromredis(redis_data):
    return redis_data[0]

def getUserNamefromSessionData(json_data):
    base_prefixhomepath = '/iplant/home/'
    response = ResponseParser.parse_getUserDetails(json_data[1])
    homepath = response.base_paths.user_home_path
    return homepath.split(base_prefixhomepath)[1]

def getHomePath_Prefx(username, json_data):
    response = ResponseParser.parse_getUserDetails(json_data[1])
    for root in response.roots:
        if root.label == username:
            return root.path.split(username)[0]

def getCommunityData_Prefix(username, json_data):
    response = ResponseParser.parse_getUserDetails(json_data[1])
    for root in response.roots:
        if root.label == 'Community Data':
            return root.path