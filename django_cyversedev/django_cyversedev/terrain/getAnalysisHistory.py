
# get analysis history
import requests
from .. import settings
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder


def getAnalysisHistory(limit, offset, sortfield, sortdir,redis_sessiondata):
    accesstoken = str(redis_sessiondata[0])
    json_data_user = str(redis_sessiondata[1])
    #req_url = 'https://de.cyverse.org/terrain/analyses?limit='+limit+'"&offset="'+offset+'&sort-field='+sortfield+'&sort-dir='+sortdir
    req_url = 'https://de.cyverse.org/terrain/analyses?limit=10&offset=0&sort-field=enddate&sort-dir=DESC'
    r = requests.get(req_url, headers={'Authorization': 'BEARER ' + accesstoken})
    response = ResponseParser.retrieve_analyses_history(r.json())
    custom_encoder = CustomEncoder()
    res = custom_encoder.encode(response)
    return res

