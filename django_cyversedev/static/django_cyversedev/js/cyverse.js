﻿tableData = [];
rpStatus = 0;
var rpStatus = 0;
var recordsLimit = 100;
var homePath = ""
var homePathforShare = ""
var homePathforCommunity = '/iplant/home/shared'
var url_search = 'searchFile/'

$(document).ready(function (e) {
    	getUserData()
    	$("#searchRender").data("enableSearchRender", false)
    	enableSearchkeyPress()
});
function signout()
{
	$.ajax({
        	method: "GET",
            	dataType: "html",
		async: "true",
		url: location.origin+"/Signout/",
	        success: function (data, status, jqXHR) {
                	window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://cyverse.bioviz.org/");
            	},
            	error: function (jqXHR, status, err) {
               	 	alert(err)
		},
            	complete: function (jqXHR, status) {
            	}
	});
}
function getUserData(){
    	$.ajax({
           	method: "GET",
            	dataType: "html",
        	async: "true",
		url: location.origin+"/getRootPathUser/",
	        success: function (data, status, jqXHR) {
                	homePath = JSON.parse(data).roots[0].path
                	homePathforCommunity = JSON.parse(data).roots[1].path
                	homePathforShare = JSON.parse(data).roots[2].path
                	getFileList(homePath, recordsLimit, 0, "NAME", "ASC", "home", true)
            	},
            	error: function (jqXHR, status, err) {
                	alert(err)
		},
            	complete: function (jqXHR, status) {
            }
	});
}
function renderSectionGetFiles(section, fromsearch){
    	var main_Section = ""
    	if (fromsearch){
        	main_Section =  $("#page-selection").data("currentSection")
        	$("#fileFolder").show();
        	$("#analysesHist").hide();
        	$("#searchRender").data("enableSearchRender", true)
    	}
    	else{
        	main_Section = section
        	$("#fileFolder").show();
	        $("#analysesHist").hide();
        	$("#searchRender").data("enableSearchRender", false)
    	}
    	if (main_Section=="home") {
        	$("#fileFolder").show();
	        $("#analysesHist").hide();
        	$("#searchText").attr("placeholder","Search in Home...")
	        getFileList(homePath, recordsLimit, 0, "NAME", "ASC", main_Section, true)
    	}
    	else if (main_Section == "community"){
        	$("#fileFolder").show();
        	$("#analysesHist").hide();
        	$("#searchText").attr("placeholder","Search in Community...")
        	getFileList(homePathforCommunity, recordsLimit, 0, "NAME", "ASC", main_Section, true)
    	}
    	else if(main_Section == "shared"){
        	$("#fileFolder").show();
	        $("#analysesHist").hide();
        	$("#searchText").attr("placeholder","Search in Shared...")
	        getFileList(homePathforShare, recordsLimit, 0, "NAME", "ASC", main_Section, true)
    	}
    	else if(section == "analyses"){
		$("#fileFolder").hide();
		$("#analysesHist").show();
		analysesHistory();
    	}
}

function disableSearchRender(){
        $("#searchRender").data("enableSearchRender", false)
}
function enableSearchkeyPress()
{
  	$( "#searchText" ).keypress(function(event) {
        	if (event.which == 13){
        		renderSectionGetFiles("", true) // on Enter press
        	}
    	});
}

function getFileList(url, limit, offset, sortcol, sortdir, section, defaultaction) {
        closerp()
        var serviceurl = ""
        var renderSearch = $("#searchRender").data("enableSearchRender")
        var baseurl=""
        var func=""
        loadingDisplay(".middleLoad",true)
        if(renderSearch){
            	var label =  $("#searchText").val()
            	var baseurl="/searchFile?url="
           	serviceurl = location.origin+baseurl+url+"&limit="+recordsLimit+"&offset="+offset+"&sortcol="+sortcol+"&sortdir="+sortdir+"&label="+label+"&type="+section
        }else {
            	$("#searchText").val("")
            	$("#searchRender").data("enableSearchRender", false)
            	if(section=="community"){
               		baseurl="/getCommunityData?url="
               		func="getCommunityData"
            	}else if(section=="shared"){
               		baseurl="/renderSharedData?url="
               		func="getShareFileList"
            	}
            	else{
                	baseurl="/middlepanel?url="
                	func="getFileList"
            	}
              	serviceurl = location.origin+baseurl+url+"&limit="+recordsLimit+"&offset="+offset+"&sortcol="+sortcol+"&sortdir="+sortdir
	}
	$.ajax({
		method: "GET",
                dataType: "html",
        	async: "true",
		url: serviceurl,
	        success: function (data, status, jqXHR) {
	             	if (data.error) {
                        	alert(data.error.message);
                	}else{
                    		$("#page-selection").data("currentPath",url)
                    		$("#page-selection").data("currentSection", section)
                    		setupPagination(JSON.parse(data), false, false, defaultaction)
	            		interpretData(JSON.parse(data), func)
	            		buildBreadCrumbs(JSON.parse(data), func)
	            		loadingDisplay(".middleLoad",false)
	            	}
            	},
            	error: function (jqXHR, status, err) {
                	alert(err)
	    	},
            	complete: function (jqXHR, status) {
            	}
	});
}
function populateTable(tableData, type) {
    	var menuButton = '<button class="btn btn-success mr-1">View in IGB</button>\n' +
        	'<button type="button" class="btn popMenu"><img src="/static/img/3dot.png" height="25" /></button>'
    	if (type == "folders") {
        	for (var data in tableData) {
            		var row = "<tr id='" + tableData[data].id + "' class='folder'><td><i class='fas fa-folder mr-1'></i>" + tableData[data].label + "</td><td>" + tableData[data].dateModified + "</td><td>-</td><td></td></tr>"
            		$('#fileFolder').children('tbody').append(row)
            		$("#" + tableData[data].id).data("info", tableData[data])
        	}
    	} else if (type = "files") {
        	for (var data in tableData) {
            		var row = "<tr  id='" + tableData[data].id + "' class='file'><td title=" + tableData[data].label + "><i class='far fa-file mr-1'></i>" + tableData[data].label + "</td><td>" + tableData[data].dateModified + "</td><td>" + tableData[data].fileSize + "</td><td>" + menuButton + "</td></tr>"
            		$('#fileFolder').children('tbody').append(row)
            		$("#" + tableData[data].id).data("info", tableData[data])
        	}
    	}
}
function loadingDisplay(loading,bool){
    	if(bool){
        	$(loading).css("display","block")
    	}else{
        	$(loading).css("display","none")
    	}
}
function complete() {
    	alert("Congratulations! The Task is Complete.")
}


function downloadFile() {
    	filepath = $("#context-menu").data("fileInfo").path
    	var urlLink = location.origin + "/setPublicLink?url=" + filepath
    	var thing = filepath;
    	var fileName = thing.split('/').pop();
    	$.ajax({
        	method: "GET",
        	url: urlLink,
        	success: function (data, status, jqXHR) {
            		window.open('https://data.cyverse.org/dav-anon' + filepath, '_blank');
        	},
        	error: function (jqXHR, status, err) {
            		alert(err);
        	},
        	complete: function (jqXHR, status) {
            	var urlLink2 = location.origin + "/remPublicLink?url=" + filepath;
            	$.ajax({
                	method: "GET",
                	url: urlLink,
                	success: function (data, status, jqXHR) {
                	},
                	error: function (jqXHR, status, err) {
                    		alert(err)
                	},
                	complete: function (jqXHR, status) {
			}
            	});
        	}
    	});
}
function ShareFile(is_anonymous) {
    	filepath = $("#context-menu").data("fileInfo").path
    	var urlLink = location.origin + "/setPublicLink?url=" + filepath +"&is_anonymous=" + is_anonymous
    	$.ajax({
            	method: "GET",
            	dataType: "html",
        	async: "true",
		url: urlLink,
	        success: function (data, status, jqXHR) {

            	},
            	error: function (jqXHR, status, err) {
                	alert(err)
		},
            	complete: function (jqXHR, status) {
            	}
	});
}
function ShareFolder(is_anonymous)
{
    	filepath = $("#context-menu").data("fileInfo").path
    	var urlLink = location.origin + "/remPublicLink?url=" + filepath +"&is_anonymous=" + is_anonymous
    	$.ajax({
            	method: "GET",
            	dataType: "html",
        	async: "true",
		url: urlLink,
	        success: function (data, status, jqXHR) {
                	ShareFile(is_anonymous)
            	},
            	error: function (jqXHR, status, err) {
                	alert(err)
		},
            	complete: function (jqXHR, status) {
            	}
	});
}
function analysesHistory(){
	loadingDisplay(".middleLoad",true)
	//$('#analysesHist').children('tbody').html("")
	$.ajax({
		method: "GET",
		dataType: "html",
		async: "true",
		url: location.origin+"/getAnalyze/",
		success: function (data, status, jqXHR) {
			$('#analysesHist').children('tbody').html("")
			$("#page-selection").data("currentPath", location.origin+"/getAnalyze/")
                        $("#page-selection").data("currentSection", "Analyses")
                        setupPagination(JSON.parse(data), false, false, true)
			analyseData=JSON.parse(data)["analyses"];
			var statusCol="";
			alert(analyseData)
			for(var obj in analyseData){
				if(analyseData[obj].status=="Completed"){
					statusCol = "table-success";
				}else if(analyseData[obj].status=="Rejected"){
					statusCol = "table-danger";
				}else if(analyseData[obj].status=="Running"){
					statusCol = "table-warning";
				}
				var row = "<tr><td>" + analyseData[obj].name + "</td><td>" +analyseData[obj].app_name + "</td><td>" +analyseData[obj].startdate + "</td><td>" +analyseData[obj].enddate + "</td><td class="+statusCol+" table-bordered>" +analyseData[obj].status + "</td></tr>"
            			$('#analysesHist').children('tbody').append(row)
			}
			loadingDisplay(".middleLoad",false)
		},
		error: function (jqXHR, status, err) {
			alert("Error: "+err)
		},
		complete: function (jqXHR, status) {
		}
	});
}
